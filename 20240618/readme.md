[TOC]

# Variables 

Para crear variables he utilizado let (antiguamente se utilizaba var).

```js
let nombreVariable=valor;
```

>Nota: Las variables se utilizan en JS para almacenar datos primitivos (texto, numeros, logicos);

# Constantes

Para crear una constante se utiliza la palabra reservada const

Las constantes como en todos los lenguajes no puedes cambiar su valor

```js
const nombreConstante=valor;
```

> Nota: Las constantes se utilizan principalmente para almacenar objetos y arrays

# Array

Es un tipo de dato (es tratado como un objeto de tipo array)

Para crear una array podemos realizarlo:
- directamente
```js
const nombreArray=[]; // array vacio
const numeros=[1,2,3]; // array con elementos
```
- la palabra reservada new

```js
const nombreArray=new Array(); // array vacio
const numeros=new Array(1,2,3); // array con elementos
const datos=new Array(10); // array de 10 elementos undefined
```

## Introducir datos al array

Para poder escribir dentro de un array

`nombreArray[posicion]=valor;`

```js
const numeros=[1,3]
numeros[5]=45; // 1,3,undefined,undefined,undefined,45
```

Si quiero escribir en el primer hueco libre que haya utilizamos el metodo push

`nombreArray.push(valor1,valor2,valor3);`

```js
const numeros=[1,2];
numeros.push(23,45); // 1,2,23,45
```

## Leer datos del array

Para poder leer un dato del array y almacenarlo en una variable

`variable=nombreArray[posicion]` 

```js
const numeros=[1,34,5];
let numero=0;

// leer el segundo numero
numero=numeros[1]; // numero vale 34
```

Si quiero leer el ultimo dato del array y almacenarlo en una variable. El dato leido del array se elimina.

`variable=nombreArray.pop()`

```js
const numeros=[1,32,13,4];
let numero=0;

// quiero leer el ultimo numero del array
numero=numeros.pop(); 
// numero=4
// numeros=[1,32,13]
```

## Saber la longitud de un array

Para conocer la longitud del array se utiliza la propiedad length

```js
const numeros=[1,45,5];

numeros.length; // 3
numeros[numeros.length-1]; // 5
```
# Trabajo con Nodos

Para js todo el documento de html se estructura en nodos que son objetos especiales de js

Estos objetos esta representados en lo que se denomina el DOM (modelo objetos del documento)

## Como acceder a un elemento existente

Para poder acceder a un elemento del body puedo utilizar los siguientes metodos

- document.querySelector : utilizamos selectores de css
- document.querySelectorAll : utilizamos selectores de css
- document.getElementById : utilizamos el id
- document.getElementsByName : utilizamos el atributo name
- document.getElementsByTagName : utilizamos el nombre de la etiqueta
- document.getElementsByClassName : utilizamos el atributo class

### querySelector

Permite acceder a un nodo del documento utilizando selectores de CSS.

Sintaxis:
`const nombre=document.querySelector("selector")`


```html
<div id="uno" class="cajita">
    Texto
</div>

<script>
const cajaDiv=document.querySelector("div");
// const cajaDiv=document.querySelector("#uno");
// const cajaDiv=document.querySelector(".cajita");
// const cajaDiv=document.querySelector("div#uno");

cajaDiv.textContent; // Texto
</script>
```

### querySelectorAll

querySelectorAll es un método de JavaScript que se utiliza para seleccionar todos los elementos del DOM (Document Object Model) que coinciden con un conjunto específico de selectores CSS. 

Este método devuelve una NodeList, que es una colección estática de elementos que coinciden con los selectores proporcionados.


Sintaxis:
`const nombre=document.querySelector("selector")`


```html
<div id="uno" class="cajita">
    Texto uno
</div>

<div id="dos" class="cajita">
    Texto dos
</div>

<script>
const cajas=document.querySelectorAll(".cajita");
// const cajaDiv=document.querySelectorAll("div");

cajasDiv[0].textContent; // Texto uno
cajasDiv[1].textContent; // Texto dos

</script>
```

## Crear Elementos

En el desarrollo web moderno, la manipulación dinámica del DOM (Document Object Model) es una habilidad esencial para crear interfaces de usuario interactivas y responsivas. Uno de los aspectos fundamentales de esta manipulación es la capacidad de crear y gestionar elementos HTML directamente desde JavaScript. Esta técnica permite a los desarrolladores agregar, modificar y eliminar elementos en una página web de manera programática, ofreciendo una flexibilidad significativa en el diseño y comportamiento de las aplicaciones web.

Crear elementos HTML desde JavaScript es útil en diversas situaciones, tales como la generación de contenido dinámico basado en datos de usuario, respuestas de servidores, o interacciones en tiempo real. A través de métodos y propiedades específicas del lenguaje, es posible construir estructuras HTML completas y adaptarlas sobre la marcha para mejorar la experiencia del usuario.

Este punto del manual está dedicado a explicar detalladamente cómo crear y manipular elementos HTML utilizando JavaScript. Abordaremos desde los conceptos básicos, como la creación y adición de elementos al DOM, hasta técnicas más avanzadas, como la manipulación de atributos y la gestión de eventos. Al final de esta sección, serás capaz de implementar dinámicamente componentes HTML en tus proyectos web, optimizando la interacción y funcionalidad de tus aplicaciones.


Para crear una alerta con tonalidades de bg-primary similar a Bootstrap, puedes usar los siguientes estilos en HTML embebido en Markdown. Aquí tienes un ejemplo:

markdown
Copiar código
<div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; background-color: #88c5ff; border-color: #88daff; color: white;">
Objetivos:  
</div>

- Entender los métodos básicos de creación de elementos HTML en JavaScript.
- Aprender a añadir y posicionar nuevos elementos dentro del DOM.
- Modificar atributos y contenido de los elementos creados.
- Manejar eventos y añadir interactividad a los elementos dinámicamente generados

## Principales metodos crear elementos

| **Devuelve**                         | **Método**                         | **Descripción**                                                         |
|--------------------------------------|------------------------------------|-------------------------------------------------------------------------|
| <span style="color: orange;">**ELEMENT**</span>  | `.createElement(tag, options)` | Crea y devuelve el elemento HTML definido por el <span style="color: yellow;">**STRING**</span> `tag`. |
| <span style="color: gray;">**NODE**</span>      | `.createComment(text)`          | Crea y devuelve un nodo de comentarios HTML `<!-- text -->`.            |
| <span style="color: gray;">**NODE**</span>      | `.createTextNode(text)`         | Crea y devuelve un nodo HTML con el texto `text`.                       |
| <span style="color: gray;">**NODE**</span>      | `.cloneNode(deep)`              | Clona el nodo HTML y devuelve una copia. `deep` es `false` por defecto. |
| <span style="color: red;">**BOOLEAN**</span>    | `.isConnected`                  | Indica si el nodo HTML está insertado en el documento HTML.             |
