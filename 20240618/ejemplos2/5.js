/*
Crear un párrafo con texto y añadirlo a un div con id contenedor
Para crear los elementos utilizar createElement
Para insertarlos utilizar appendChild
para añadir texto al parrafo utilizar textContent
<p>
  Texto
</p>
*/

// apuntar al div (un nodo de tipo element)
// este div esta en el DOM
const divContenedor = document.querySelector('#contenedor');

// crear los elementos

// crear un parrafo (un nodo de tipo element)
// este parrafo no esta en el DOM
const p = document.createElement('p');

// añadir texto al parrafo
p.textContent = 'Texto';

// otra forma de añadir el texto al parrafo
// crear un nodo de tipo texto
// const texto = document.createTextNode('Texto');
// insertar el nodo de tipo texto en el parrafo
// p.appendChild(texto);


// añadir el parrafo con el texto al DOM
// añadir al div

divContenedor.appendChild(p);

