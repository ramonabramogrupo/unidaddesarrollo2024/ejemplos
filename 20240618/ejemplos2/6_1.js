/*
Crea una lista desordenada con tres elementos: 
"Elemento 1", "Elemento 2", "Elemento 3" 
y añádela al div con id "salida".

Crear una funcion denominada lista que reciba como argumento un array y devuelva un objeto ul
*/

function lista(elementos) {
    const ul = document.createElement("ul");

    elementos.forEach(function (elemento) {
        const li = document.createElement("li");
        li.textContent = elemento;
        ul.appendChild(li);
    });
    return ul;
}

const salida = document.querySelector("#salida");

salida.appendChild(lista(["Elemento 1", "Elemento 2", "Elemento 3"]));