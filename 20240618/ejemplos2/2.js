/*
 * cada vez que realizamos un click en la pantalla
 * hay que crear un div cuadrado en el punto pulsado
 */

// escuchador a todo el document

document.addEventListener('click', function (event) {

    // creo un par de variables para almacenar las coordenadas del evento
    let x = event.clientX;
    let y = event.clientY;

    // constante que apunte a los elementos que queremos utilizar
    const cuerpo = document.querySelector('body');

    // creo un nuevo div
    const div = document.createElement('div');

    // coloca la clase cuadrado al div
    div.classList.add('cuadrado');

    // coloco las coordenadas en el div
    div.style.top = y + 'px';
    div.style.left = x + 'px';

    // dibujar el div (colocandose a un elemento del DOM).
    // En este caso el body. 
    cuerpo.appendChild(div);


});



