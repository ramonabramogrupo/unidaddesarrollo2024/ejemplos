/*
 * cada vez que realizamos movemos el raton en la pantalla
 * hay que crear un div cuadrado  si tenemos el boton izquierdo del raton pulsado
 */

document.addEventListener('mousemove', function (event) {
    const boton = event.buttons;
    if (boton == 1) {
        const div = document.createElement('div');
        let coordenadaX = event.clientX;
        let coordenadaY = event.clientY;
        div.style.left = coordenadaX + 'px';
        div.style.top = coordenadaY + 'px';
        div.classList.add('cuadrado');
        document.body.appendChild(div);
    }
});