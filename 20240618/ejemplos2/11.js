/*
Funcion que recibe un array bidimensional de datos 
y devuelve un elemento con la cabecera de la tabla
Mostrar los datos de los alumnos en una tabla 
Añade la tabla al div con id salida.

const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es']
    ['pedro', 'garcia', 'pedro@alpe.es']
];

<table>
<thead>
    <tr>
        <td>nombre</td>
        <td>apellidos</td>
        <td>email</td>
    </tr>
</thead>
<tbody>
    <tr>
        <td>jose</td>
        <td>perez</td>
        <td>jose@alpe.es</td>
    </tr>
    <tr>
        <td>maria</td>
        <td>lopez</td>
        <td>maria@alpe.es</td>
    </tr>
    <tr>
        <td>pedro</td>
        <td>garcia</td>
        <td>pedro@alpe.es</td>
    </tr>
</tbody>
</table>

*/

