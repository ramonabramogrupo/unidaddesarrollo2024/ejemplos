/*
quiero que cada vez que pulsamos sobre el boton añadir debe crear un nuevo enlace en el menu
de navegación
utilizar funciones anonimas
utilizar createElement
utilizar appendChild
textContent o innerHTML
*/

// crear constantes y variables
const boton = document.querySelector('button');

// escuchadores de eventos
boton.addEventListener('click', function (event) {
    // constantes

    // accedo al ul para poder crear li dentro
    const menu = document.querySelector('ul');
    // constantes que me apunten a los input
    const inputEnlace = document.querySelector('#enlace');
    const inputTexto = document.querySelector('#titulo');


    // menu.innerHTML += `
    //                     <li class="nav-item">
    //                     <a class="nav-link" href="${inputEnlace.value}">${inputTexto.value}</a>
    //                 </li>
    // `;

    // crear un nuevo li
    const li = document.createElement('li');

    // crear un nuevo a
    const a = document.createElement('a');

    // añadir al li la clase
    //li.className = 'nav-item';
    li.classList.add('nav-link');

    // añadir a la a la clase
    a.classList.add('nav-link');

    //añadir a la a el href
    a.href = inputEnlace.value;

    // añadir a la a el contenido
    a.textContent = inputTexto.value;
    /**
     <li class="nav-item"></li>
     <a class="nav-link" href="${inputEnlace.value}">${inputTexto.value}</a>
     */

    // colocar la a dentro del li

    li.appendChild(a);

    // colocar el li dentro del ul
    menu.appendChild(li);

});






