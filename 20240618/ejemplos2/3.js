/*
 * cada vez que realizamos un click en la pantalla
 * hay que crear un div cuadrado en el punto pulsado
 * 
 * cada uno de los divs sea de un color aleatorio
 * 
 * crear una funcion denominada color que retorne un color en RGB
 * de forma aleatoria
 */

/**
 * Retorna un color de forma aleatoria en formato RGB
 * @returns {string}
 */
function color() {
    let color = "#";
    // #F0BC12
    for (let c = 0; c < 6; c++) {
        let numero = Math.floor(Math.random() * 16); // numero del 0 al 15
        numero = numero.toString(16); // numero en formato hexadecimal
        color += numero;
    }
    return color;

}


// function color() {
//         let color = "#";
//         // Generar valores aleatorios para R, G y B entre 0 y 255
//         const r = Math.floor(Math.random() * 256);
//         const g = Math.floor(Math.random() * 256);
//         const b = Math.floor(Math.random() * 256);

//         // Retornar el color en formato RGB
//         color = `rgb(${r}, ${g}, ${b})`;

//         return color;
//     }


document.addEventListener('click', function (event) {

    // creo un par de variables para almacenar las coordenadas del evento
    let x = event.clientX;
    let y = event.clientY;

    // constante que apunte a los elementos que queremos utilizar
    const cuerpo = document.querySelector('body');

    // creo un nuevo div
    const div = document.createElement('div');

    // coloca la clase cuadrado al div
    div.classList.add('cuadrado');
    //div.className = "cuadrado";

    // coloco las coordenadas en el div
    div.style.top = y + 'px';
    div.style.left = x + 'px';
    div.style.backgroundColor = color();

    // dibujar el div (colocandose a un elemento del DOM).
    // En este caso el body. 
    cuerpo.appendChild(div);
});

