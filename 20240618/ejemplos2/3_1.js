// Colocar un escuchador a cada una de las celdas de la tabla
// y vaciar el contenido de la celda pulsada
// utilizar event.targert y event.currentTarget

// 1 opcion 
// Colocar el escuchador a la tabla

// 2 opcion
// Colocar el escuchador a las celdas



// colocar escuchadores
const tabla = document.querySelector('table');

const celdas = document.querySelectorAll('td');

// tabla.addEventListener('click', function (event) {
//     // console.log(event.target); // referencia a la celda
//     // console.log(event.currentTarget); // referencia a la tabla
//     // event.target.innerHTML = "";
//     //event.currentTarget.innerHTML = "";
// });

celdas.forEach(function (celda) {
    celda.addEventListener('click', function (event) {
        console.log(event.target); // celda
        console.log(event.currentTarget); // celda
        //event.target.innerHTML = "";
        event.currentTarget.innerHTML = "";
    });
});