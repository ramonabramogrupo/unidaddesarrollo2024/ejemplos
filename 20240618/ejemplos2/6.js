/*
Crea una lista desordenada con tres elementos: 
"Elemento 1", "Elemento 2", "Elemento 3" 
y añádela al div con id "salida".
*/

const divSalida = document.querySelector('#salida');

const lista = ["Elemento 1", "Elemento 2", "Elemento 3"];

const ul = document.createElement('ul');

lista.forEach(function (elemento) {
    const li = document.createElement('li');
    li.textContent = elemento;
    ul.appendChild(li);
});

divSalida.appendChild(ul);