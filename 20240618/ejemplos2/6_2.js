/*
Crea una lista desordenada con tres elementos: 
"Elemento 1", "Elemento 2", "Elemento 3" 
y añádela al div con id "salida".

Crear una funcion denomina dibujarLista que reciba como argumento un array un objeto element. Debe crear un objeto ul con los li de los elementos pasados y dibujarlo en el element pasado
*/


function dibujarLista(lista, elemento) {
    const ul = document.createElement('ul');
    lista.forEach((elementoLista) => {
        const li = document.createElement('li');
        li.textContent = elementoLista;
        ul.appendChild(li);
    });
    elemento.appendChild(ul);
}

const divSalida = document.querySelector('#salida');

dibujarLista(["Elemento 1", "Elemento 2", "Elemento 3"], divSalida);
