[TOC]

# Ejercicios

## Ejercicio 1

Quiero que cada vez que pulsamos sobre el boton añadir debe crear un nuevo enlace en el menu de navegación colocado en la web
- utilizar funciones anonimas
- utilizar createElement
- utilizar appendChild
- textContent o innerHTML

## Ejercicio 2

Cada vez que realizamos un click en la pantalla hay que crear un div cuadrado en el punto pulsado
- utilizar funciones anonimas
- utilizar createElement
- utilizar appendChild

## Ejercicio 3

Cada vez que realizamos un click en la pantalla hay que crear un div cuadrado en el punto pulsado.

Cada uno de los divs sea de un color aleatorio

Crear una funcion denominada color que retorne un color en RGB de forma aleatoria.

## Ejercicio 3-1

Colocar un escuchador a cada una de las celdas de la tabla
y vaciar el contenido de la celda pulsada
utilizar event.targert y event.currentTarget

- 1 opcion : Colocar el escuchador a la tabla
- 2 opcion : Colocar el escuchador a las celdas

## Ejercicio 4

Cada vez que realizamos movemos el raton en la pantalla hay que crear un div cuadrado  si tenemos el boton izquierdo del raton pulsado

