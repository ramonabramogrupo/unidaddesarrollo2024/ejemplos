/*
Crea una tabla con dos filas y tres columnas. 
Cada celda debe contener el texto "Fila X, Columna Y" donde X es el número de la fila e Y el número de la columna. 
Añade la tabla al div con id salida.
*/

const divSalida = document.querySelector('#salida');

const tabla = document.createElement('table');


for (let nfila = 0; nfila < 2; nfila++) {
    // creando filas
    const fila = document.createElement('tr');

    for (let ncolumna = 0; ncolumna < 3; ncolumna++) {
        // creando celdas
        const celda = document.createElement('td');
        celda.textContent = `Fila ${nfila + 1}, Columna ${ncolumna + 1}`;
        // anadiendo celda a la fila
        fila.appendChild(celda);
    }
    // anadiendo fila a la tabla
    tabla.appendChild(fila);
}

// meter la tabla en el div Salida para colocarla en el DOM
divSalida.appendChild(tabla);

