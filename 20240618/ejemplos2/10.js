/*
Crear una funcion que reciba un array bidimensional de datos
y devuelva una estructura dinamica de una tabla 
generada a partir del array de datos

const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es']
    ['pedro', 'garcia', 'pedro@alpe.es']
];

<table>
    <tr>
        <td>nombre</td>
        <td>apellidos</td>
        <td>email</td>
    </tr>
    <tr>
        <td>jose</td>
        <td>perez</td>
        <td>jose@alpe.es</td>
    </tr>
    <tr>
        <td>maria</td>
        <td>lopez</td>
        <td>maria@alpe.es</td>
    </tr>
    <tr>
        <td>pedro</td>
        <td>garcia</td>
        <td>pedro@alpe.es</td>
    </tr>
</table>

*/

function crearTabla(datos) {
    const tabla = document.createElement('table');

    return tabla;

}



const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es']
    ['pedro', 'garcia', 'pedro@alpe.es']
];

const divSalida = document.querySelector('#salida');

divSalida.appendChild(crearTabla(alumno));
