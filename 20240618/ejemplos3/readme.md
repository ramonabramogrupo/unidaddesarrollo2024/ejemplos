[TOC]

# Introducción a los Tipos Primitivos y Objetos

JavaScript es un lenguaje de programación flexible y dinámico que permite trabajar con diferentes tipos de datos. 

Estos tipos de datos se dividen principalmente en dos categorías: 
- tipos primitivos 
- objetos. 

Esta división es fundamental para entender cómo se manejan y manipulan los datos en JavaScript.

# Tipos Primitivos en JavaScript

Los tipos primitivos son los datos más básicos y simples en JavaScript. 

Estos incluyen:

- Number: Representa valores numéricos, tanto enteros como decimales.
- String: Representa secuencias de caracteres.
- Boolean: Representa valores lógicos, true o false.
- Null: Representa la ausencia intencional de cualquier valor.
- Undefined: Indica que una variable ha sido declarada pero no inicializada.
- BigInt: Permite representar enteros de tamaño arbitrario.
- Symbol: Representa un valor único y anónimo.

```js
let numero = 42;          // Number
let texto = "Hola";       // String
let esVerdadero = true;   // Boolean
let nada = null;          // Null
let indefinido;           // Undefined
let granNumero = 123n;    // BigInt
let simbolo = Symbol();   // Symbol

```

## Características de los Tipos Primitivos

### Inmutabilidad
Los tipos primitivos son inmutables, lo que significa que su valor no puede cambiar una vez que se han creado. Cualquier operación que parezca modificar un primitivo en realidad crea y devuelve un nuevo valor.

```js
let texto = "Hola";
texto[0] = "J";  // Intento de modificar el primer carácter
console.log(texto);  // "Hola" (no cambia)

let nuevoTexto = texto.toUpperCase();  // Crea un nuevo string
console.log(nuevoTexto);  // "HOLA"
console.log(texto);  // "Hola" (permanece igual)

```

### Sin Propiedades Propias: 
Los tipos primitivos no pueden tener propiedades o métodos agregados directamente. Cualquier intento de hacerlo no tiene efecto.

```js
let numero = 42;
numero.propiedadNueva = "nuevo";
console.log(numero.propiedadNueva);  // undefined
```
# Objetos en JavaScript

En JavaScript, un objeto es una colección de miembros, y un miembro es una asociación entre un nombre (o clave) y un valor. 

Los valores de los miembros pueden ser:
- datos, en cuyo caso los miembros se denominan propiedades 
- funciones, en cuyo caso los miembros se conocen como métodos.

En JavaScript, casi todo lo que usamos son objetos. 

>Un objeto es una colección de datos y funciones que están agrupados en una sola entidad. Esto lo hace muy flexible y poderoso para manejar datos.

