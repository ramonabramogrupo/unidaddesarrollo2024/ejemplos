/*
cuando pulse el boton me escriba el texto del input en el div salida
dentro de un h1 si el boton es h1
dentro de un h2 si el boton es h2
dentro de un h3 si el boton es h3
no quiero utilizar innerHTML para añadir contenido a los elementos
para crear elementos createElement
para añadir esos elementos a etiquetas utilizar appendChild
para añadir texto textContent
o contenido con HTML innerHTML
*/

// Opcion 1

// voy utilizar los nodos (botones) por separado

// apunto al boton que el usuario puede pulsar
const boton1 = document.querySelector('#boton1');
const boton2 = document.querySelector('#boton2');
const boton3 = document.querySelector('#boton3');


// listener
boton1.addEventListener('click', function (event) {
    // apunto al div de salida
    const divSalida = document.querySelector('#salida');

    // apunto al input
    const inputTexto = document.querySelector('#texto');

    // constante apunta a un nuevo nodo que creo de tipo h1
    const cajah1 = document.createElement('h1');

    // obtengo el valor del input
    let texto = inputTexto.value;

    // añadir en el div Salida el h1
    divSalida.appendChild(cajah1);

    // escribo el valor del input en el h1
    cajah1.textContent = texto;


});

boton2.addEventListener('click', function (event) {
    // apunto al div de salida
    const divSalida = document.querySelector('#salida');

    // apunto al input
    const inputTexto = document.querySelector('#texto');

    // constante que cree un nuevo h1
    const cajah2 = document.createElement('h2');

    // obtengo el valor del input
    let texto = inputTexto.value;

    // añadir en el div Salida el h1
    divSalida.appendChild(cajah2);

    // escribo el valor del input en el h1
    cajah2.textContent = texto;

});

boton3.addEventListener('click', function (event) {
    // apunto al div de salida
    const divSalida = document.querySelector('#salida');

    // apunto al input
    const inputTexto = document.querySelector('#texto');

    // constante que cree un nuevo h1
    const cajah3 = document.createElement('h3');

    // obtengo el valor del input
    let texto = inputTexto.value;

    // añadir en el div Salida el h1
    divSalida.appendChild(cajah3);

    // escribo el valor del input en el h1
    cajah3.textContent = texto;
});

// // opcion con array de botones
// // opcion 2
// const botones = document.querySelectorAll('button');
// botones.forEach(function (boton) {
//     boton.addEventListener('click', function (event) {
//         // apunto al boton pulsado
//         const botonPulsado = event.target;

//         // leo el contenido del boton
//         let tipo = botonPulsado.dataset.tipo;

//         // apunto al div de salida
//         const divSalida = document.querySelector('#salida');

//         // apunto al input
//         const inputTexto = document.querySelector('#texto');

//         // constante que cree un nuevo elemento
//         const caja = document.createElement(tipo);

//         // obtengo el valor del input
//         let texto = inputTexto.value;

//         // añadir en el div Salida el h1
//         divSalida.appendChild(caja);

//         // escribo el valor del input en el h1
//         caja.textContent = texto;


//     });
// });





















