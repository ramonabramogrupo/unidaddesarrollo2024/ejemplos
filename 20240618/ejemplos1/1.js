/*
cuando pulse el boton me escriba el texto del input en el div salida
*/

// apunto al boton que el usuario puede pulsar
const boton = document.querySelector('button');


// listener
boton.addEventListener('click', function (event) {
    // apunto al div de salida
    const divSalida = document.querySelector('#salida');

    // apunto al input
    const inputTexto = document.querySelector('#texto');

    // obtengo el valor del input
    let texto = inputTexto.value;

    // escribo el valor del input en el div
    // divSalida.innerHTML = texto;
    divSalida.textContent = texto;
});








