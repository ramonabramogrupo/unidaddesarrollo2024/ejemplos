const elementos = [
    "elemento 1",
    "elemento 2",
    "elemento 3",
    "elemento 4",
    "elemento 5",
];

const divContenedor = document.querySelector("#contenedor");

// esto es un fragmento creado desde una etiqueta template de html
const plantilla = document.querySelector("template").content;

// menu es un fragmento
const menu = plantilla.cloneNode(true);

// apuntar al ul del menu
// ahora tengo un element
const ul = menu.querySelector("ul");


elementos.forEach(function (elemento, indice) {
    const li = (indice == 0) ? menu.querySelector("li") : menu.querySelector("li").cloneNode(true);
    li.textContent = elemento;
    ul.appendChild(li);

});

// renderizo el fragmento
divContenedor.appendChild(menu);







