/*
Quiero crear en html la siguiente estructura de elementos

<div class="ejemplo ejemplo1 mt-2"id="miDiv">
    <p>Texto uno</p>
    <p>Este es<br>un párrafo.</p>
    <a href="http://www.google.es">Ir a google</a>
</div>

createElement
appendChild
textContent
innerHTML
querySelector
querySelectorAll
*/

// creo constantes que apuntan a elementos que he creado
const miDiv = document.createElement('div');
const p1 = document.createElement('p');
const p2 = document.createElement('p');
const a = document.createElement('a');

// modificar los atributos de los elementos
miDiv.id = 'miDiv';
miDiv.classList.add('ejemplo', 'ejemplo1', 'mt-2');
// miDiv.setAttribute('class', 'ejemplo ejemplo1 mt-2');
// miDiv.className = 'ejemplo ejemplo1 mt-2';
a.href = 'http://www.google.es';

// modificar el contenido de los elementos
p1.textContent = 'Texto uno';
p2.innerHTML = 'Este es<br>un párrafo.';
a.textContent = 'Ir a google';

// meter en miDiv los elementos
miDiv.appendChild(p1);
miDiv.appendChild(p2);
miDiv.appendChild(a);

// mostrar la nueva estructura creada en el body
document.body.appendChild(miDiv);


