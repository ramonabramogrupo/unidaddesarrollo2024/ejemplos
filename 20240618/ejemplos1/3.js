/*
funcion llamada dibujar que crea la siguiente estructura de html
con creacion de nodos

<div id="miDiv">
    <p>titulo</p>
    <p>contenido</p>
    <a href="enlace">texto</a>
</div>
dentro del div salida
createElement
appendChild
textContent
innerHTML
querySelector
querySelectorAll
*/

function dibujar(titulo, contenido, enlace, texto) {
    // caja donde voy a dibujar la estructura
    const divSalida = document.querySelector('#salida');

    // creo los elementos que necesito
    const miDiv = document.createElement('div');
    const p1 = document.createElement('p');
    const p2 = document.createElement('p');
    const a = document.createElement('a');

    // modificando los elementos creados
    miDiv.id = 'miDiv';
    p1.textContent = titulo;
    p2.textContent = contenido;
    a.href = enlace;
    a.textContent = texto;

    // colocar todas las etiquetas dentro de miDiv
    miDiv.appendChild(p1);
    miDiv.appendChild(p2);
    miDiv.appendChild(a);

    // colocar en el div de salida miDiv
    divSalida.appendChild(miDiv);


}

dibujar('Alpe', 'Probando contenido dinamica', 'http://www.google.es', 'Ir a google');
dibujar('Alpe', 'Probando contenido dinamica', 'http://www.google.es', 'Ir a google');

