


// variables y constantes

let nombresFotos = ["foto1.jpg", "foto2.jpg", "foto3.jpg",
    "foto4.jpg", "foto5.jpg", "foto6.jpg"];
nombresFotos[100] = "foto7.jpg";


// constante que apunte a los objetos del DOM que quiero utilizar

// apuntando al h1
const elementoH1 = document.querySelector("h1");

// apuntando al div donde voy a colocar las fotos
const divFotos = document.querySelector("#fotos");


// modificando el contenido de la etiqueta h1
elementoH1.innerHTML = "Ejemplo de clase 1";

// modificando el estilo de la etiqueta h1
// elementoH1.style.width = "300px";
// elementoH1.style.height = "50px";
// elementoH1.style.backgroundColor = "gray";
// elementoH1.style.borderRadius = "20px";
// elementoH1.style.padding = "10px";
// elementoH1.style.lineHeight = "50px";
// elementoH1.style.textAlign = "center";

// modifico el estilo de la etiqueta h1 cambiandole la clase
elementoH1.classList.add("titulo2");
//elementoH1.className = "titulo2";
//elementoH1.setAttribute("class", "titulo2");

// opcion 1
// utilizando innerHTML sin bucle

// divFotos.innerHTML = `<img src="imgs/${nombresFotos[0]}">`;
// divFotos.innerHTML += `<img src="imgs/${nombresFotos[1]}">`;
// divFotos.innerHTML += `<img src="imgs/${nombresFotos[2]}">`;
// divFotos.innerHTML += `<img src="imgs/${nombresFotos[3]}">`;

//opcion 2
// utilizando bucles


// utilizando for
// for (let contador = 0; contador < nombresFotos.length; contador++) {
//     // comprobar que el elemento del array no esta undefined
//     if (nombresFotos[contador] != undefined) {
//         divFotos.innerHTML += `<img src="imgs/${nombresFotos[contador]}">`;
//     }
// }

// utilizando for of
// for (const nombreFoto of nombresFotos) {
//     if (nombreFoto != undefined) {
//         divFotos.innerHTML += `<img src="imgs/${nombreFoto}">`;
//     }
// }

// utilizando forEach
// nombresFotos.forEach(function (nombreFoto) {
//     divFotos.innerHTML += `<img src="imgs/${nombreFoto}">`;
// });

// utilizando arrow function
// nombresFotos.forEach((nombreFoto) => {
//     divFotos.innerHTML += `<img src="imgs/${nombreFoto}">`;
// });

// podria crear elementos (nodos)

// opcion 1

// sin bucle utilizar createElement y appendChild

// const fotos = [];

// creo la primera imagen
// fotos[0] = document.createElement("img");
// fotos[0].src = "imgs/" + nombresFotos[0];
// // añado la imagen al div
// divFotos.appendChild(fotos[0]);

// // creo la segunda imagen
// fotos[1] = document.createElement("img");
// fotos[1].src = "imgs/" + nombresFotos[1];
// // añado la imagen al div
// divFotos.appendChild(fotos[1]);

// // creo la tercera imagen
// fotos[2] = document.createElement("img");
// fotos[2].src = "imgs/" + nombresFotos[2];
// // añado la imagen al div
// divFotos.appendChild(fotos[2]);

// opcion 2
// con bucle

// bucle forEach
nombresFotos.forEach((nombreFoto) => {
    const foto = document.createElement("img");
    foto.src = "imgs/" + nombreFoto;
    divFotos.appendChild(foto);
});

