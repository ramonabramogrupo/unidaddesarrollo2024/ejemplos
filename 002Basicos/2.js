/*
crear la funcion cambiar
*/

function cambiar(color) {
    // constante que apunte al h1
    const h1 = document.querySelector('h1');

    // cambia el color del h1
    h1.style.color = color;
}


/*
Crear la funcion mostrar
*/
function mostrar() {

    // constante que apunte al h1
    const h1 = document.querySelector('h1');

    // mostrar el h1
    h1.style.opacity = '1';

}

/*
Crear la funcion ocultar
*/

function ocultar() {

    // constante que apunte al h1
    const h1 = document.querySelector('h1');

    // ocultar el h1
    h1.style.opacity = '0';
}

/*
Crear la funcion tecla
*/

function tecla(event) {

    // constante que apunte al h1
    const h1 = document.querySelector('h1');

    // escriba el caracter pulsado en el teclado
    // en el h1
    h1.innerHTML += event.key;

}
