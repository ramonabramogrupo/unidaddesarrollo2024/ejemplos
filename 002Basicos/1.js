// constantes

// apunto a los objetos del DOM 
// etiquetas de html que quiero manejar desde JS
const h1 = document.querySelector("h1");
const div = document.querySelector("#contenido");

// salida
h1.innerHTML += " Ramon";
div.innerHTML += "<br> Texto desde JS";
div.style.backgroundColor = "gray";
div.style.width = "200px";