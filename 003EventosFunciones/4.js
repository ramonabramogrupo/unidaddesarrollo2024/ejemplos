/**
 * 
 */

// constante que apunte al boton
const btn = document.querySelector("#enviar");

// escuchador al boton
btn.addEventListener("click", function (event) {
    // constantes que apunten a los elementos del DOM
    // que quiero utilizar

    const inputPoblacion = document.querySelector("#poblacion");
    const inputProvincia = document.querySelector("#provincia");
    const ul = document.querySelector("#salida");
    const botones = document.querySelector("#botones");
    const boton = document.createElement("button");
    const h2 = document.querySelector("h2");

    // limpio el ul
    ul.innerHTML = "";

    // creo un array para colocar todos los li
    const elementosli = [];

    // en el primer li coloco la poblacion escrita
    elementosli[0] = document.createElement("li");
    elementosli[0].innerHTML = inputPoblacion.value;
    // mostrar el primer li dentro del ul
    ul.appendChild(elementosli[0]);

    // en el segundo li coloco la provincia escrita
    elementosli[1] = document.createElement("li");
    elementosli[1].innerHTML = inputProvincia.value;
    // mostrar el segundo li dentro del ul
    ul.appendChild(elementosli[1]);


    // podia ahorrar la creacion de nodos y utilizar innerHTML
    // ul.innerHTML="<li>"+inputPoblacion.value+"</li>";

    // muestro el boton de enviar todo en botones
    botones.appendChild(boton);
    boton.innerHTML = "Enviar todo";
    h2.innerHTML = "Los datos introducidos son:";

});