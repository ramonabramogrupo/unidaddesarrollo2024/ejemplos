/*

*/

// constantes que apunten a los botones
const btnMostrar = document.querySelector("#mostrar");
const btnOcultar = document.querySelector("#ocultar");


// evento click para el boton mostrar
btnMostrar.addEventListener("click", mostrar);

// evento click para el boton ocultar
btnOcultar.addEventListener("click", ocultar);

// evento keydown para el body
document.addEventListener("keydown", tecla);

function mostrar() {
    // constantes que apunten a los elementos del formulario
    const color = document.querySelector("#color");
    const texto = document.querySelector("#texto");
    const ancho = document.querySelector("#ancho");
    const alto = document.querySelector("#alto");

    // constantes que apunten a los elementos del display
    const display = document.querySelector("#display");

    // formateo el display con los valores del formulario
    display.style.backgroundColor = color.value;
    display.style.color = texto.value;
    display.style.width = ancho.value + "px";
    display.style.height = alto.value + "px";
    display.style.opacity = 1;
}

function ocultar() {

    // constantes que apunten a los elementos del display
    const display = document.querySelector("#display");

    // ocultar el display
    display.style.opacity = 0;

}

function tecla(event) {

    // constantes que apunten a los elementos del display
    const display = document.querySelector("#display");

    // escriba el caracter pulsado en el teclado
    // en el display
    display.innerHTML += event.key;
}


