/**
 * 
 */

// constantes que apunten a los botones
const botones = document.querySelectorAll("button");

// constante que apunta al select
const select = document.querySelector("select");

// eventos click sobre los botones
for (const boton of botones) {
    boton.addEventListener("click", function (event) {
        const display = document.querySelector("#display");
        display.style.color = event.target.dataset.color;
    });
}

// evento change del select
select.addEventListener("change", function (event) {
    // contantes que apunte a los elementos de html que quiero utilizar
    const h2 = document.querySelector("h2");
    const display = document.querySelector("#display");

    let indice = 0;
    let texto = "";
    let fecha = new Date(); // es un objeto de tipo date
    let salida = "";


    // event.target.value saco el valor de la opcion seleccionada
    indice = event.target.selectedIndex;
    texto = event.target.options[indice].text;

    // mostrar en h2 el valor de la opcion seleccionada
    h2.innerHTML = texto;

    // mostrar en display la echa actual formateada de acuerdo
    // a la opcion seleccionada
    switch (indice) {
        case 1:
            // salida = `${fecha.getDate()}/${(fecha.getMonth() + 1)}/${fecha.getFullYear()}`;

            // salida=fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear();

            // utilizando formato de fecha
            salida = fecha.toLocaleDateString("es-ES");
            break;

        case 2:
            // salida = `${fecha.getHours()}:${fecha.getMinutes()}:${fecha.getSeconds()}`;

            // utilizando formato de fecha
            salida = fecha.toLocaleTimeString("es-ES");
            break;

        case 3:
            // salida = `${fecha.getDate()}/${(fecha.getMonth() + 1)}/${fecha.getFullYear()} ${fecha.getHours()}:${fecha.getMinutes()}:${fecha.getSeconds()}`;

            // utilizando formato de fecha
            // salida = fecha.toLocaleString();
            salida = fecha.toLocaleDateString("es-ES") + " " + fecha.toLocaleTimeString("es-ES");
            break;

        case 4:
            salida = fecha.getDay();
            break;

        case 5:
            salida = fecha.getMonth();
            break;

        case 6:
            salida = fecha.getFullYear();
            break;
        default:
            salida = "";
            break;
    }

    display.innerHTML = salida;

});








