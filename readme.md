# Manual Javascript

JavaScript (JS) es un lenguaje de programación interpretado, principalmente utilizado en el desarrollo web para crear contenido interactivo y dinámico en las páginas web. 

Originalmente fue desarrollado por Netscape como un complemento para su navegador web, y hoy en día es una de las tecnologías fundamentales de la web junto con HTML y CSS. 

JavaScript permite a los desarrolladores implementar funcionalidades complejas, como actualizaciones en tiempo real, animaciones, manejo de eventos del usuario y mucho más, directamente en el navegador del usuario. 

Además, JavaScript se utiliza en el desarrollo del lado del servidor gracias a entornos como Node.js.

![js](imgs/1.png)

| Tecnología | Definición                                                                                  |
|------------|---------------------------------------------------------------------------------------------|
| HTML       | Lenguaje de marcado utilizado para estructurar y presentar contenido en la web.             |
| CSS        | Lenguaje de hojas de estilo utilizado para describir la apariencia y el formato de un documento escrito en HTML.|
| JavaScript | Lenguaje de programación utilizado para crear contenido web interactivo y dinámico.         |



En una pagina basica

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo 1</title>
    <style>
       /* aqui coloco todo lo que es CSS */
    </style>
</head>

<body>
    <div>
        <h1>Ejemplo 1</h1>
        <div id="contenido" onclick="desaparecer()">
            Ejemplo de clase
        </div>
    </div>
</body>
<script>
    /* aqui coloco lo que es Javascript */
</script>
</html>
```

## Ejemplo basico

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo 1</title>
    <style>
    /*
        Decora el contenido de la web
    */
        h1 {
            /* color de fuente */
            color: blue;

        }

        #contenido {
            /* color de fondo */
            background-color: gray;
            /* margen interno */
            padding: 10px;
            /* ancho */
            width: 300px;
            /* margen externo (arriba y abajo, izquierda y derecha con auto te centra) */
            margin: 10px auto;

        }
    </style>
</head>

<body>
    <div>
        <h1>Ejemplo 1</h1>
        <div id="contenido" onclick="desaparecer()">
            Ejemplo de clase
        </div>
    </div>
</body>
<script>
    /*
     * Cuando el usuario pulsa en el div desaparece
     */ 
    function desaparecer() {
        document.querySelector('#contenido').style.display = 'none';
    }
</script>

</html>
```

# Separar el contenido en tres archivos

Yo creo un archivo html y dentro del html mediante las etiquetas:
- link : añades archivo con css
- script : añades archivo con js


```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo 1</title>

    <!-- Para añadir estilos utilizo link -->
    <link rel="stylesheet" href="archivo.css">
</head>

<body>
    <div>
        <h1>Ejemplo 1</h1>
        <div id="contenido" onclick="desaparecer()">
            Ejemplo de clase
        </div>
    </div>
</body>

<!-- Para añadir contenido de JS -->
<script src="archivo.js"></script>

</html>
```